# Final_Interview

Create a chat story web application with php, jascription/jquery, html, css. See the video for reference and test_story.json is the story file reference. You should show the text when user clicks on the screen, one at a time. Refer to the same design as the video. Use PHP, javascript/jquery, html css to create this application

You can use html,css and javascript only if you dont want to use php. Message should show up on mouse click by the user

Data Example
```
{
   "dynamicResponse":0,
   "selection":0,
   "headerOnly":0,
   "isVideo":0,
   "message":"Hey hey, hows it going? Not seen you since the party!",
   "isAudio":"0",
   "index":0,
   "isMedia":0,
   "author":"Sarah",
   "dynamicResponseEnds":0,
   "source":0
}
```

"source" 0 is incoming message and 1 is outgoing message
"message" is text message
"authore" is the owner of the message

If you are using Windows,
Download WAMP to start a server locally for testing
Mac : 
MAMP


This is an example of what you are trying to replicate
https://drive.google.com/file/d/13xh_l9PTiaO3lDyOAfFaMDWOy-bdOLHf/view